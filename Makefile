SHELL = /bin/bash

build:
	@docker build -t dockerhub/node .

push: build
	@docker tag dockerhub/sshd registry.gitlab.com/dockerhub/node
	@docker push registry.gitlab.com/dockerhub/node

