FROM node:9.10.0-alpine

RUN apk update	&& apk upgrade && apk add openssh git \
		&& rm -rf /var/cache/apk/* /tmp/*
